let thief1 = {name:"Protagonist",codeName:"Joker",persona:"Arsene,Satanael",arcana:"The Fool",img:"https://vignette.wikia.nocookie.net/megamitensei/images/6/63/Persona_5_Hero.png/revision/latest?cb=20160505182242"};
let thief2 = {name:"Ryuji Sakamoto",codeName:"Skull",persona:"Captain Kidd",arcana:"The Chariot",img:"https://vignette.wikia.nocookie.net/megamitensei/images/b/bc/Ryuji_Sakamoto.png/revision/latest?cb=20160505182138"};
let thief3 = {name:"Morgana",codeName:"Mona",persona:"Zorro,Mercurius",arcana:"The Magician",img:"https://vignette.wikia.nocookie.net/megamitensei/images/6/68/P5_Morgana_character_artwork.png/revision/latest?cb=20160505181742"};
let thief4 = {name:"Ann Takamaki",codeName:"Panther",persona:"Carmen, Hecate",arcana:"The Lovers",img:"https://vignette.wikia.nocookie.net/megamitensei/images/b/be/An_takamaki.png/revision/latest?cb=20170426203909"};
let thief5 = {name:"Yusuke Kitagawa",codeName:"Fox",persona:"Goemon Kamu Susano-o",arcana:"The Emperor",img:"https://vignette.wikia.nocookie.net/megamitensei/images/5/5c/Yusuke-Kitagawa.png/revision/latest/scale-to-width-down/112?cb=20160505181913"};

let theifs = [thief1,thief2,thief3,thief4,thief5];
let teamList = [];
let selectedThief = "";

window.onload = function(){
    fillThiefData();
}

function fillThiefData(){
    let teamList = document.getElementById("teamList");
    let list = "<ul>";

    for(let thief of theifs){
        list += "<li id=\"" + thief.name + "\" onclick=\"showDetails('" + thief.name + "')\">" + thief.name + "</li>";
    }

    list+= "</ul>";

    teamList.innerHTML = list;
}

function showDetails(name){
    addClassToSelected(name);
    addToTeam(name);
    let profile = document.getElementById("profile");
    let list = "<ul>";

    for(let thief of theifs){
        if(thief.name == name){
            list+= "<li><b>Name:</b> " + thief.name + "</li>";
            list += "<li><b>Code Name:</b> " + thief.codeName + "</li>"; 
            list += "<li><b>Persona:</b> " + thief.persona + "</li>"; 
            list += "<li><b>Arcana: </b> " + thief.arcana + "</li>"; 
            list += "<li><img src=\"" + thief.img + "\" alt=\""+ thief.name +"\"></li>";
            break;
        }
    }

    profile.innerHTML = list;
}

function addToTeam(name){

    if(!teamList.includes(name) && teamList.length < 3){
        teamList.push(name);
        showTeam();
    }
}

function showTeam(){
    
    let team = document.getElementById("team");
    let text = "<ul>";

    for(let list of teamList){
        text += "<li onclick=\"remove('" + list + "')\">" + list + "</li>";   
    }

    team.innerHTML = text;
}

function remove(name){
    teamList.splice(teamList.indexOf(name),1);
    showTeam();
}

function addClassToSelected(name){
    let thief = document.getElementById(selectedThief);
    
    if(thief != null){
        thief.classList.remove("selected");
    }

    thief = document.getElementById(name);    
    thief.classList.add("selected");
    selectedThief = name;
}