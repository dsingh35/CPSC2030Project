<?php
    require_once 'sqlhelper.php';

    $connection = connectToMyDatabase();
    
        $user = json_decode($_POST["myData"],true);
        
        $name = $user["name"];
        $action = $user["action"];
        
        if(!strcmp($action,"store")){
            $msg = $user["msg"];                
            $result = $connection->query("call postMessage('$name','$msg')");        
        }
        else if(!strcmp($action,"all")){
            $time = $user["time"];
            $result = $connection->query("call getAll(\"$time\")");
            $data = $result->fetch_all(MYSQLI_ASSOC);
        }
        else{
            $result = $connection->query("call getLast()");
            $data = $result->fetch_all(MYSQLI_ASSOC);
        }

        $json = json_encode($data);
        echo $json;

    clearConnection($connection);   
?>
