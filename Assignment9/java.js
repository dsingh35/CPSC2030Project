"use strict"
 
let lastMessageTime = "2017-07-24 12:21:00";

function send() {
    let username = $(".user input").val();
    let message = $(".input input").val();
    let user = {
        name : username,
        action : "store",
        msg : message
    };
    user = JSON.stringify(user);

    $.ajax({
        url: "server.php/",
        type: 'POST',
        dataType: "JSON",
        data: {myData : user},
        success: (data) => {
        console.log(data);
        alert("got the data");
        },
        error: (err) => {
        console.log(err);
        }
    });
}

setInterval(() => {
    let username = $(".user input").val();
    let user = {
        name : username,
        action: "all",
        time: lastMessageTime
    };
    user = JSON.stringify(user);

    $.ajax({
        url: "server.php",
        type: 'POST',
        dataType: "JSON",
        data: {myData : user},
        success: (data) => {
        console.log(data);
        let container = $(".messages");
        data.forEach(element => {
            let name = $("<div>",{
                class: "users"
            });
            let p = $("<p>",{
                class: "msg"
            });
            let b = $("<b>",{
                class: "name"
            });
            let br = $("<br>");

            b.html(element.username);
            b.appendTo(p);
            br.appendTo(p);
            p.append(element.msg);
            p.appendTo(name);
            name.appendTo(container);
            lastMessageTime = element.messageTime;
        });
        },
        error: (err) => {
        console.log(err);
        }
    });
},5000);

$(document).ready(() => {
    let username = $(".user input").val();
    let user = {
        name : username,
        action: "last"
    };
    user = JSON.stringify(user);

    $.ajax({
        url: "server.php",
        type: 'POST',
        dataType: "JSON",
        data: {myData : user},
        success: (data) => {
        console.log(data);
        let container = $(".messages");
        data.forEach(element => {
            let name = $("<div>",{
                class: "users"
            });
            let p = $("<p>",{
                class: "msg"
            });
            let b = $("<b>",{
                class: "name"
            });
            let br = $("<br>");

            b.html(element.username);
            b.appendTo(p);
            br.appendTo(p);
            p.append(element.msg);
            p.appendTo(name);
            name.appendTo(container);
            lastMessageTime = element.messageTime;
        });
        },
        error: (err) => {
        console.log(err);
        }
    });
});