DELIMITER //
CREATE PROCEDURE addUser(IN userName text,IN pass text,IN email text)
BEGIN
    INSERT INTO users VALUES (userName, pass,email);
    INSERT INTO userdetails(userName,email) VALUES (userName,email);    
END //

DELIMITER //
CREATE PROCEDURE morning()
BEGIN
    SELECT *
    FROM morningtime;
END //

DELIMITER //
CREATE PROCEDURE getAllQA()
BEGIN    
    SELECT qa.id,question,answer
    FROM qa LEFT JOIN Ans ON qa.id = Ans.id;
END //


DELIMITER //
CREATE PROCEDURE createQuestion(IN userQ text,IN Uquestion text)
BEGIN
    INSERT INTO qa(user,question)
    VALUES(userQ,Uquestion);
END //

DELIMITER //
CREATE PROCEDURE answerQuestion(IN QId INTEGER, IN userQ text,IN Uquestion text)
BEGIN
    INSERT INTO Ans(id,user,answer)
    VALUES(QId,userQ,Uquestion);
END //

DELIMITER //
CREATE PROCEDURE shenmu()
BEGIN
    SELECT *
    FROM shenmu;
END //

DELIMITER //
CREATE PROCEDURE chushan()
BEGIN
    SELECT *
    FROM chushan;
END //


DELIMITER //
CREATE PROCEDURE updateUser(IN name text,IN f text,IN l text,IN ph text,IN residency text)
BEGIN
    UPDATE userdetails 
    SET Residency = residency, FirstName = f, LastName = l,
    Phone = ph
    WHERE userName = name;
END //

DELIMITER //
CREATE PROCEDURE getUser(IN username text,IN passw text)
BEGIN
    SELECT *
    FROM users
    WHERE userName LIKE CONCAT('%',username,'%') AND pass LIKE CONCAT('%',passW,'%');
END //

DELIMITER //
CREATE PROCEDURE getUserDetails(IN name text)
BEGIN
    SELECT *
    FROM userdetails
  	WHERE userName = name;
END //

DELIMITER //
CREATE PROCEDURE deleteUser(IN username text)
BEGIN
    DELETE 
    FROM userdetails
  	WHERE userName = name;
    
    DELETE 
    FROM users
  	WHERE userName = name;
END //