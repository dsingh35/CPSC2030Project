CREATE DATABASE Alishan;

CREATE TABLE MorningTime(
    day text,
    jan text,
    feb text,
    march text,
    april text,
    may text,
    june text,
    july text,
    august text,
    sept text,
    oct text,
    nov text,
    decem text
);

CREATE TABLE QA(
   id INTEGER UNIQUE NOT NULL AUTO_INCREMENT,
    user text,
    question text,
);

CREATE TABLE Ans(
    id INTEGER,
    user text,
    answer text
);

CREATE TABLE Shenmu(
    train Integer,
    alishanDept text,
    shenmuArrival text,
    returnTrain Integer,
    shenmuDept text,    
    alishanArrival text
);

CREATE TABLE Chushan(
    train Integer,
    alishanDept text,
    chushanArrival text,
    returnTrain Integer,
    chushanDept text,    
    alishanArrival text
);

CREATE TABLE Users(
    userName char(6) UNIQUE NOT NULL,
    pass text NOT NULL,
    email text NOT NULL
);

CREATE TABLE UserDetails(
    userName char(6) UNIQUE NOT NULL,
    FirstName text,
    LastName text,
    Email text NOT NULL,
    Phone text,
    Residency text,
);
