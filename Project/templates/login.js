window.onload = () => {
    let user = {
        action : "get"
    };
    
    user = JSON.stringify(user);

    $.ajax({
        url: "../sqlhelper.php/",
        type: 'POST',
        data : {user : "get"},
        success : (data) => {
            $('#user').html(data);
            console.log(data);
            if("Log in" == data.trim()){
                $('#user').attr("href", "../login.php");
                $('#action').html("Sign up");
                $('#action').attr("href", "../signup.php");
            }
            else{
                $('#user').attr("href", "../profile.php");
                $('#action').html("Log out");
                $('#action').attr("href", "../logout.php");
            }    
        },
        error : (error) => {
            alert("error occurrred");
            console.log(error);
        } 
    })

}

function logIn(){
    let name = $('#loginName').val();
    let pass = $('#password').val();
    
    let user = {
        Uname : name,
        Upass : pass
    };

    user = JSON.stringify(user);

    $.ajax({
        url: "../login.php/",
        type: 'POST',
        data : {userData : user},
        success : (data) => {
            $('#user').html(data);
            console.log(data);
            if(data == "Log in"){
                $('#user').attr("href", "login.html");
                $('#action').html("Sign up");
                $('#action').attr("href", "Signup.html");
            }
            else{
                $('#user').attr("href", "profile.html");
                $('#action').html("Log out");
                $('#action').attr("href", "logout.html");
            }    
        },
        error : (error) => {
            alert("error occurrred");
            console.log(error);
        } 
    })

}