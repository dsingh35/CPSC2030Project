<?php
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';

    $twig = setupMyTwigEnvironment();
    $connection = connectToMyDatabase();
    $template = $twig->load('profile.html');
    $name = getSessionUser();

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $first = $_POST["first"];
        $last = $_POST["last"];
        $phone = $_POST["phone"];
        $address = $_POST["address"];

        $result = $connection->query("call updateUser(\"$name\",\"$first\",\"$last\",\"$phone\",\"$address\")");
        clearConnection($connection);
        $result = $connection->query("call getUserDetails(\"$name\")");
        clearConnection($connection);

        if($result){
            $data = $result->fetch_all(MYSQLI_ASSOC);
            if($data){
                echo $template->render(array("items" => checkProfileLogIn(),"profile" => $data));                
            }else {
                echo $template->render(checkLogIn());    
            }
        }
    }   
 
    if(strcmp($name,"Log in") != 0){
        $result = $connection->query("call getUserDetails(\"$name\")");
        if($result){
            $data = $result->fetch_all(MYSQLI_ASSOC);
            if($data){
                echo $template->render(array("items" => checkProfileLogIn(),"profile" => $data));                
            }else {
                echo $template->render(checkLogIn());    
            }
        }
    }
    else {
        echo $template->render(checkLogIn());    
    }
?>
    