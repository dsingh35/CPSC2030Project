<?php
session_start();
if(!isset($_SESSION["loggedIn"])){
    $_SESSION["loggedIn"] = "Log in";
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST["user"])){
    $user = $_POST["user"];

   if(strcmp($user,"get") == 0){
        echo $_SESSION["loggedIn"];
    }
    else {
        echo "error";
    }
}

function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}
//helper function
function wrap($tag,$value) { //expect strings for both parameters
    return "<$tag>$value</$tag>";
}

//DB setup for this web-app
function connectToMyDatabase(){
    $user = 'CPSC2030';
    $pwd = 'CPSC2030';
    $server = 'localhost';
    $dbname = 'alishan';
 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}
//twig setup for this web-app
function setupMyTwigEnvironment(){
    $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
    $twig = new Twig_Environment($loader); 
    return $twig;  
}

function logOut(){
        $_SESSION["loggedIn"] = "Log in";
}

function dumpErrorPage($twig){
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed"));
}

function checkLogIn(){
    $notLogin = array( "Log in" => "login.php", "Sign up" => "signup.php");
    if(strcmp($_SESSION["loggedIn"],"Log in") == 0){
        return array("items" => $notLogin);        
    }
    else{
        $logged = array($_SESSION["loggedIn"] => "profile.php", "Log out" => "logout.php");
        return array("items" => $logged);
    }
}

function checkProfileLogIn(){
    $notLogin = array( "Log in" => "login.php", "Sign up" => "signup.php");
    if(strcmp($_SESSION["loggedIn"],"Log in") == 0){
        return $notLogin;        
    }
    else{
        $logged = array($_SESSION["loggedIn"] => "profile.php", "Log out" => "logout.php");
        return $logged;
    }
}

function getSessionUser(){
    return $_SESSION["loggedIn"];
}

function signUp($name){
    $_SESSION["loggedIn"] = $name;
}
?>

