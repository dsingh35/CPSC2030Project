function askQuestion(){
    $('#ask').addClass("none");
    let i = $('<input>',{
        type: "text",
        placeholder : "enter your question here",
        id : "question"
    });

    let br = $("<br>");
    let br2 = $("<br>");

    let b = $('<input>',{
        type : "button",
        value : "submit",
        onclick : "submitQuestion()"
    });

    let parent = $('.input')[0];
    console.log(i);
    i.appendTo(parent);
    br.appendTo(parent);    
    br2.appendTo(parent);    
    b.appendTo(parent);
}

function submitQuestion(){
    let question = $('#question').val();
    let u = $('#user').html();
    let ques = {
        user : u,
        q : question
    };

    ques = JSON.stringify(ques);

    $.ajax({
        url: "qa.php",
        type: 'POST',
        data : {myData : ques},
        success : (data) => {
            console.log(data);
        },
        error : (error) => {
            alert("error occurrred");
            console.log(error);
        } 
    })
}

function answer(id) {
    let bas = document.getElementById("b" + id);
    bas.classList.add("none");

    let i = $('<input>',{
        type: "text",
        placeholder : "enter your answer here",
        id : "answer"
    });

    let br = $("<br>");
    let br2 = $("<br>");

    let b = $('<input>',{
        type : "button",
        value : "submit",
        onclick : "submitAnswer('" + id + "')"
    });

    let parent = $('body').find('#d' + id); 
    i.appendTo(parent);
    br.appendTo(parent);    
    br2.appendTo(parent);    
    b.appendTo(parent);    
}

function submitAnswer(id){
    let qId  = $('#' + id).html();
    let answer = $('#d' + id + " #answer").val();
    console.log(answer); 

    let ans = {
        id : qId,
        a : answer
    };

    ans = JSON.stringify(ans);

    $.ajax({
        url: "qa.php",
        type: 'POST',
        data : {myAns : ans},
        success : (data) => {
            console.log(data);
            window.location.reload(true);
        },
        error : (error) => {
            alert("error occurrred");
            console.log(error);
        } 
    })
}