<?php
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';

    $twig = setupMyTwigEnvironment();
    $conection = connectToMyDatabase();
    $template = $twig->load('questions.html');

    if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST["myData"])){
        $ques = json_decode($_POST["myData"],true);
        $user = "david";
        $question = $ques["q"];

        $result = $conection->query("call createQuestion(\"$user\",\"$question\")");
        clearConnection($conection);
        if($result){
                echo "succed";
        }
        else{
            echo "failed";
        }
    }
    else if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST["myAns"])){
        $ans = json_decode($_POST["myAns"],true);
        $user = "david";
        $id = $ans["id"];
        $ans = $ans["a"];

        $result = $conection->query("call answerQuestion(\"$id\",\"$user\",\"$ans\")");
        clearConnection($conection);
        if($result){
            echo "succeed";
        }
        else{
            echo "failed";
        }

    }
    else{
        $result = $conection->query("call getAllQA()");
        $data = $result->fetch_all(MYSQLI_ASSOC);

        echo $template->render(array("ques" => $data,"items" => checkProfileLogIn())); 
    }
?>