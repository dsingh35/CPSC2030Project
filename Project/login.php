<?php
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';

    $twig = setupMyTwigEnvironment();
    $conection = connectToMyDatabase();
    $template = $twig->load('Login.html');
    
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $name = $conection->real_escape_string( $_POST['loginName']);
        $pass = $conection->real_escape_string( $_POST['password']);
        $result = $conection->query("call getUser(\"$name\",\"$pass\")");
        clearConnection($conection);
        $row = $result->fetch_all(MYSQLI_ASSOC);
        if(count($row)){
            foreach ($row as $item) {
                $_SESSION["loggedIn"] = $item["userName"];                
            }
        }
    }
    echo $template->render(checkLogIn());
?>
