<?php
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';

    $twig = setupMyTwigEnvironment();
    $connection = connectToMyDatabase();
    $template = $twig->load('logout.html');

    $user = getSessionUser();
    logOut();

    echo $template->render(array("items" => checkProfileLogIn(),"user" => $user));
?>
    