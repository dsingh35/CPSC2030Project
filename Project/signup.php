<?php
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';

    $twig = setupMyTwigEnvironment();
    $conection = connectToMyDatabase();
    $template = $twig->load("Signup.html");

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $name = $conection->real_escape_string( $_POST['username']);
        $pass = $conection->real_escape_string($_POST["password"]);
        $email = $conection->real_escape_string($_POST["email"]);
        $result = $conection->query("call addUser(\"$name\",\"$pass\",\"$email\")");
        if($result){
            signUp($name);
        }
    }

    echo $template->render(checkLogIn());
    $conection->close();
?>