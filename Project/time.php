<?php
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';
 
    $twig = setupMyTwigEnvironment();
    $conection = connectToMyDatabase();
    $template = $twig->load('time.html');

    $result = $conection->query("call morning()");
    clearConnection($conection);
    $morning = $result->fetch_all(MYSQLI_ASSOC);

    $result = $conection->query("call chushan()");
    clearConnection($conection);   
    $chushan = $result->fetch_all(MYSQLI_ASSOC);
    
    $result = $conection->query("call shenmu()");
    clearConnection($conection);
    $shenmu = $result->fetch_all(MYSQLI_ASSOC);
    
   echo $template->render(array("morning" => $morning, "chushan" => $chushan, "shenmu" => $shenmu,"items" => checkProfileLogIn()));
?>
