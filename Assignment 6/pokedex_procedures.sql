DELIMITER // 
CREATE PROCEDURE pokemon_list()
BEGIN
    SELECT *
    FROM pokedex;
END //

DELIMITER // 
CREATE PROCEDURE getPopular()
BEGIN
   SELECT DISTINCT names From pokedex
ORDER BY bst DESC LIMIT 30;
END //

DELIMITER //
CREATE PROCEDURE showPokemon(IN str text)
BEGIN
    SELECT * FROM pokedex
    FULL JOIN powers ON FULL.types LIKE CONCAT('%',powers.types,'%')
    AND FULL.names LIKE CONCAT('%',str,'%');
END //
     
DELIMITER //
CREATE PROCEDURE getPokedex(IN str text)
BEGIN
    SELECT * 
    FROM pokedex
    WHERE names LIKE CONCAT('%',str,'%');
END //