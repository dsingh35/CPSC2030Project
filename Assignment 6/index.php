<?php
session_start();
    require_once 'sqlhelper.php';
    require_once './vendor/autoload.php';

    /*change the line below*/
    $pageDomain = "http://localhost/Project/Assignment%206/index.php";
    $exists = false;
    $twig = setupMyTwigEnvironment();
    $conection = connectToMyDatabase();
    $result = $conection->query("call getPopular()");
    clearConnection($conection);
    $links = $result->fetch_all(MYSQLI_ASSOC);

    if(!isset($_SESSION["favourites"])){
       $_SESSION["favourites"] = array();
    }   
    $template = $twig->load('main.twig.html');
    echo $template->render(array("head" => "My Favourite Pokemon Page"));
    
    $template = $twig->load('sidebar.twig.html');    
    echo $template->render(array("links" => $links));

    $template = $twig->load('card.twig.html');
    $move = ' ';
    $name = ' ';
    if(isset($_GET["move"]) && $_GET["name"]){
        $move = mysqli_real_escape_string($conection,$_GET["move"]);
        $name = mysqli_real_escape_string($conection,$_GET["name"]);
    }

    $i=0;
    foreach($_SESSION["favourites"] as $element) {
        foreach ($element as $item) {
            if(strcmp($name,$item["names"])== 0){
                $exists = true;  
                if(strcmp($move,"delete") == 0){
                    array_splice($_SESSION["favourites"],$i,1);
                }
                break;
            }       
        }
        $i++;    
    }

    if(strcmp($move,"add") == 0 && count($_SESSION["favourites"]) < 5 && $exists == false){
        $result = $conection->query("call getPokedex(\"$name\")");  
        clearConnection($conection);

        if($result){
            $items = $result->fetch_all(MYSQLI_ASSOC);
            array_push($_SESSION["favourites"],$items);
        }
        else{
            dumpErrorPage();
        }
    }
    
    foreach ($_SESSION["favourites"] as $items) {
        echo $template->render(array("items" => $items,"link" => $pageDomain));
    }

    $result = $conection->query("call pokemon_list()");  
    clearConnection($conection);

    if($result){
        $items = $result->fetch_all(MYSQLI_ASSOC);
        $template = $twig->load('pokemon.twig.html');    
        echo $template->render(array("pokemons" => $items,"link" => $pageDomain));
    }
    else{
        dumpErrorPage();
    }
?>